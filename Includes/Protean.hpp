#ifndef FRAO_PROTEAN_TOPINCL
#define FRAO_PROTEAN_TOPINCL

//! \file
//!
//! \brief Header of entire Protean api
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Functor/Delegate.hpp"
#include "Lifetime/OwnerPointer.hpp"
#include "Structures/CircularBuffer.hpp"
#include "Structures/ContiguousArray.hpp"
#include "Structures/RankQueue.hpp"
#include "Utility/DummyParam.hpp"
#include "Version/VersionIncl.hpp"

#endif