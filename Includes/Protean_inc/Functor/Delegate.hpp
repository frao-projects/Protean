#ifndef FRAO_PROTEAN_FUNCTOR_DELEGATE
#define FRAO_PROTEAN_FUNCTOR_DELEGATE

#include <Nucleus_inc/Environment.hpp>
#include <memory>  //for std::shared_ptr
#include <vector>  //for std::vector

//! \file
//!
//! \brief Header for Delegate class, and associated functor
//! classes
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

namespace frao
{
//! \class BasePtr
//!
//! \brief Base class for other functor classes
//!
//! \tparam ReturnType Return type of the function that this
//! type is representing
//!
//! \tparam ArgTypes parameter types of the function that
//! this type is representing (all of the parameters, in
//! order)
//!
//! \todo some kind of functor ID, so that users can
//! identify what ptr this is, if they have several in an
//! array (ie: for delegate class)
//!
//! \todo make ReturnType not part of the type of BasePtr,
//! if possible? (so that delegates can store functions of
//! varying return type)
template<class ReturnType, class... ArgTypes>
class BasePtr
{
   public:
	typedef BasePtr<ReturnType, ArgTypes...> ThisType;

	//! \brief standard (default) constructor
	BasePtr() = default;
	//! \brief Deleted copy constructor
	BasePtr(const ThisType&) = delete;
	//! \brief Deleted move constructor
	BasePtr(ThisType&&) = delete;

	//! \brief Deleted copy assignment operator
	ThisType& operator=(const ThisType&) = delete;
	//! \brief Deleted move assignment operator
	ThisType& operator=(ThisType&&) = delete;

	//! \brief standard (default) destructor, virtual so
	//! that other classes can safely derive from this one
	virtual ~BasePtr() noexcept = default;

	//! \brief calls the function stored, and returns the
	//! function's result.
	//!
	//! \returns The return value of the called function
	//!
	//! \param[in] args parameter pack that will be passed
	//! to the function
	virtual ReturnType operator()(
		ArgTypes... args) noexcept = 0;
};

//! \class NormPtr
//!
//! \brief functor class for 'normal' functions. That is,
//! non-member functions (specifically, functions without an
//! implicit 'this')
//!
//! \tparam ReturnType Return type of the function that this
//! type is representing
//!
//! \tparam ArgTypes parameter types of the function that
//! this type is representing (all of the parameters, in
//! order)
//!
//! \todo specify other parts of function signature -
//! noexcept / const etc. But
//! **not** return type - we want to type erase that, if
//! possible
template<class ReturnType, class... ArgTypes>
class NormPtr final
	: public BasePtr<ReturnType, ArgTypes...>
{
	ReturnType (*m_Func)(ArgTypes...) noexcept;

   public:
	typedef NormPtr<ReturnType, ArgTypes...> ThisType;

	//! \brief constructor that creates from a (non-member)
	//! function pointer
	//!
	//! \param[in] func A (non-member) function pointer that
	//! points to the function that will be stored in this
	//! object
	NormPtr(
		ReturnType (*func)(ArgTypes...) noexcept) noexcept
		: BasePtr<ReturnType, ArgTypes...>(), m_Func(func)
	{
		runtime_assert(func != nullptr);
	}

	//! \brief standard (defaulted) copy constructor
	NormPtr(const ThisType&) = default;

	//! \brief standard (defaulted) move constructor
	NormPtr(ThisType&&) = default;

	//! \brief standard (defaulted) destructor, not declared
	//! virtual since this class is final, and hence cannot
	//! be derived from
	~NormPtr() noexcept = default;

	//! \brief standard (defaulted) copy assignment operator
	//!
	//! \returns *this, for chaining (ie: obj1 = obj2 =
	//! func)
	ThisType& operator=(const ThisType&) = default;

	//! \brief standard (defaulted) move assignment operator
	//!
	//! \returns *this, for chaining (ie: obj1 = obj2 =
	//! std::move(func))
	ThisType& operator=(ThisType&&) = default;

	//! \brief calls the stored function, with the provided
	//! argument(s), and returns what the function returns
	//!
	//! \returns The return value of the called function
	//!
	//! \param[in] args parameter pack that will be passed
	//! to the function
	ReturnType operator()(
		ArgTypes... args) noexcept override
	{
		return m_Func(args...);
	}
};

//! \class ClassPtr
//!
//! \brief functor class for member functions. That is,
//! functions that have an implicit 'this' pointer as an
//! argument. These require different treatment from
//! 'normal' functions, and hence have a different functor
//! class to service their needs
//!
//! \tparam Classname The name of the class, that the
//! function in question is a part of. ie: The type of the
//! implicit 'this' pointer
//!
//! \tparam ReturnType The type that the function in
//! question returns
//!
//! \tparam ArgTypes Parameter pack, of the argument types
//! of the function in question. The argument types of the
//! function must be provided in full and in order
//!
//! \todo specify other parts of function signature -
//! noexcept / const etc. But
//! **not** return type - we want to type erase that, if
//! possible
template<class Classname, class ReturnType,
		 class... ArgTypes>
class ClassPtr final
	: public BasePtr<ReturnType, ArgTypes...>
{
	ReturnType (Classname::*m_Func)(ArgTypes...) noexcept;
	Classname* m_CallObject;

   public:
	using ThisType =
		ClassPtr<Classname, ReturnType, ArgTypes...>;
	using BaseType = BasePtr<ReturnType, ArgTypes...>;

	//! \brief construct a functor from a member function
	//! pointer, and an object on which to call it.
	//!
	//! \param[in] func The member function that this
	//! functor should refer to
	//!
	//! \param[in] object the object on which to call the
	//! member function ptr.
	//! **Will not own the object. Must stay in scope for
	//! the lifetime of this object** ie: the caller should
	//! ensure that the object remains valid for the
	//! duration of this functor's lifetime
	ClassPtr(
		ReturnType (Classname::*func)(ArgTypes...) noexcept,
		Classname* object) noexcept
		: BaseType(), m_Func(func), m_CallObject(object)
	{
		runtime_assert(func != nullptr);
		runtime_assert(object != nullptr);
	}

	//! \brief standard (defaulted) copy constructor
	ClassPtr(const ThisType&) = default;

	//! \brief standard (defaulted) move constructor
	ClassPtr(ThisType&&) = default;

	//! \brief standard (defaulted) destructor. Not declared
	//! as virtual, since this class is final, and may not
	//! derived from
	~ClassPtr() noexcept = default;

	//! \brief standard (defaulted) copy assignment operator
	ThisType& operator=(const ThisType&) = default;

	//! \brief standard (defaulted) move assignmenet
	//! operator
	ThisType& operator=(ThisType&&) = default;

	//! \brief call the member function, on the previously
	//! specified object, and return the result of that
	//! function call
	//!
	//! \returns The return value of the called function
	//!
	//! \param[in] args parameter pack that will be passed
	//! to the function
	ReturnType operator()(
		ArgTypes... args) noexcept override
	{
		return (m_CallObject->*m_Func)(args...);
	}
};

//! \class Delegate
//!
//! \brief class that stores a number of function pointers,
//! in order to call them at once (conceptually at once, not
//! necessarily physically)
//!
//! \tparam ReturnType The return type of the functions to
//! be passed
//!
//! \tparam ArgTypes the arguments types of the functions to
//! be stored, in full and in order (except any implict this
//! pointer, for member function ptrs)
//!
//! \todo add delegate iterator, const_iterator,
//! reverse_iterator
template<class ReturnType, class... ArgTypes>
class Delegate final
{
	std::vector<
		std::shared_ptr<BasePtr<ReturnType, ArgTypes...>>>
		m_PtrList;

   public:
	//! \brief construct empty delegate
	Delegate() : m_PtrList()
	{}

	//! \brief construct delegate from (normal) function
	//! pointer
	//!
	//! \param[in] function (normal) function pointer to
	//! create delegate from (ie: function pointer will be
	//! one of the functions stored in the delegate)
	Delegate(ReturnType (*function)(ArgTypes...) noexcept)
		: m_PtrList()
	{
		addCallback(function);
	}

	//! \brief constuct delegate from (member) function
	//! pointer
	//!
	//! \param[in] function (member) function pointer to
	//! create delegate from (ie: member function pointer
	//! will be on of the functions stored in the delegate)
	//!
	//! \param[in] callobject object on which to call this
	//! member function pointer (and this one only). **must
	//! remain in scope for the duration of use of the
	//! corresponding stored member function**
	template<class Classname>
	Delegate(ReturnType (Classname::*function)(
				 ArgTypes...) noexcept,
			 Classname* callobject)
		: m_PtrList()
	{
		addCallback(function, callobject);
	}

	//! \brief add a function to the delegate, to called
	//! when the others are
	//!
	//! \returns unsigned integer indicating the position of
	//! the function in the delegate's array of functions
	//!
	//! \param[in] func shared_ptr pointing to the baseptr
	//! of the functor that hold the function
	natural64 addCallback(
		std::shared_ptr<BasePtr<ReturnType, ArgTypes...>>
			func)
	{
		runtime_assert(func);

		m_PtrList.push_back(func);
		return m_PtrList.size() - 1;
	}

	//! \brief add a (normal) function pointer to the
	//! delegate, to called when the others are
	//!
	//! \returns unsigned int indicating the position of the
	//! function in the delegate's array of functions
	//!
	//! \param[in] function (normal) function pointer to be
	//! stored in this delegate
	natural64 addCallback(
		ReturnType (*function)(ArgTypes...) noexcept)
	{
		m_PtrList.push_back(
			std::make_shared<
				NormPtr<ReturnType, ArgTypes...>>(
				function));

		return m_PtrList.size() - 1;
	}

	//! \brief add a (normal) function pointer to the
	//! delegate, to called when the others are
	//!
	//! \returns unsigned int indicating the position of the
	//! function in the delegate's array of functions
	//!
	//! \param[in] func shared_ptr to NormPtr holding
	//! (normal) function pointer, to be stored in this
	//! delegate
	natural64 addCallback(
		std::shared_ptr<NormPtr<ReturnType, ArgTypes...>>
			func)
	{
		runtime_assert(func);

		m_PtrList.push_back(func);
		return m_PtrList.size() - 1;
	}

	//! \brief add a (member) function pointer to the
	//! delegate, to called when the others are
	//!
	//! \returns unsigned int indicating the position of the
	//! function in the delegate's array of functions
	//!
	//! \param[in] function (member) function pointer to be
	//! stored in this delegate
	//!
	//! \param[in] callobject object on which to call the
	//! member function pointer
	template<class Classname>
	natural64 addCallback(ReturnType (Classname::*function)(
							  ArgTypes...) noexcept,
						  Classname* callobject)
	{
		m_PtrList.push_back(
			std::make_shared<ClassPtr<Classname, ReturnType,
									  ArgTypes...>>(
				function, callobject));

		return m_PtrList.size() - 1;
	}

	//! \brief add a (member) function pointer to the
	//! delegate, to called when the others are
	//!
	//! \returns unsigned int indicating the position of the
	//! function in the delegate's array of functions
	//!
	//! \param[in] func shared_ptr to a classptr holding a
	//! (member) function pointer to be stored in this
	//! delegate
	template<class Classname>
	natural64 addCallback(
		std::shared_ptr<
			ClassPtr<Classname, ReturnType, ArgTypes...>>
			func)
	{
		runtime_assert(func);

		m_PtrList.push_back(func);
		return m_PtrList.size() - 1;
	}

	//! \brief remove a function from the delegate, at the
	//! specified index
	//!
	//! \param[in] index Index of the place in the array
	//! that the function to remove is stored
	void removeCallback(natural64 index)
	{
		runtime_assert(index < m_PtrList.size());

		m_PtrList.erase(m_PtrList.begin() + index);
	}

	//! \brief gets number of stored functions, in this
	//! delegate
	//!
	//! \returns unsigned integer indicating the number of
	//! the functions in the delegate
	natural64 size() const noexcept
	{
		return m_PtrList.size();
	}

	//! \brief calls all functions in the delegate, using
	//! the supplied arguments, and returns the results of
	//! all of them
	//!
	//! \returns array of all return values of the functions
	//!
	//! \param[in] args
	//!
	//! \todo once we have functor ids, return them with
	//! each return value, so that we know what returned
	//! what
	std::vector<ReturnType> operator()(ArgTypes... args)
	{
		std::vector<ReturnType> result;

		for (auto& ptr : m_PtrList)
		{
			result.emplace_back(ptr->operator()(args...));
		}

		return result;
	}
};

//! \class Delegate<void, ArgTypes>
//!
//! \brief Delegate specialisation, for functions that
//! return void
template<class... ArgTypes>
class Delegate<void, ArgTypes...> final
{
	std::vector<std::shared_ptr<BasePtr<void, ArgTypes...>>>
		m_PtrList;

   public:
	//! \brief construct empty delegate
	Delegate() : m_PtrList()
	{}

	//! \brief construct delegate from (normal) function
	//! pointer
	//!
	//! \param[in] function (normal) function pointer to
	//! create delegate from (ie: function pointer will be
	//! one of the functions stored in the delegate)
	Delegate(void (*function)(ArgTypes...) noexcept)
		: m_PtrList()
	{
		addCallback(function);
	}

	//! \brief constuct delegate from (member) function
	//! pointer
	//!
	//! \param[in] function (member) function pointer to
	//! create delegate from (ie: member function pointer
	//! will be on of the functions stored in the delegate)
	//!
	//! \param[in] callobject object on which to call this
	//! member function pointer (and this one only). **must
	//! remain in scope for the duration of use of the
	//! corresponding stored member function**
	template<class Classname>
	Delegate(
		void (Classname::*function)(ArgTypes...) noexcept,
		Classname* callobject)
		: m_PtrList()
	{
		addCallback(function, callobject);
	}

	//! \brief add a function to the delegate, to called
	//! when the others are
	//!
	//! \returns unsigned integer indicating the position of
	//! the function in the delegate's array of functions
	//!
	//! \param[in] func shared_ptr pointing to the baseptr
	//! of the functor that hold the function
	natural64 addCallback(
		std::shared_ptr<BasePtr<void, ArgTypes...>> func)
	{
		runtime_assert(func);

		m_PtrList.push_back(func);
		return m_PtrList.size() - 1;
	}

	//! \brief add a (normal) function pointer to the
	//! delegate, to called when the others are
	//!
	//! \returns unsigned int indicating the position of the
	//! function in the delegate's array of functions
	//!
	//! \param[in] function (normal) function pointer to be
	//! stored in this delegate
	natural64 addCallback(
		void (*function)(ArgTypes...) noexcept)
	{
		m_PtrList.push_back(
			std::make_shared<NormPtr<void, ArgTypes...>>(
				function));

		return m_PtrList.size() - 1;
	}

	//! \brief add a (normal) function pointer to the
	//! delegate, to called when the others are
	//!
	//! \returns unsigned int indicating the position of the
	//! function in the delegate's array of functions
	//!
	//! \param[in] func shared_ptr to NormPtr holding
	//! (normal) function pointer, to be stored in this
	//! delegate
	natural64 addCallback(
		std::shared_ptr<NormPtr<void, ArgTypes...>> func)
	{
		runtime_assert(func.get() != nullptr);

		m_PtrList.push_back(func);
		return m_PtrList.size() - 1;
	}

	//! \brief add a (member) function pointer to the
	//! delegate, to called when the others are
	//!
	//! \returns unsigned int indicating the position of the
	//! function in the delegate's array of functions
	//!
	//! \param[in] function (member) function pointer to be
	//! stored in this delegate
	//!
	//! \param[in] callobject object on which to call the
	//! member function pointer
	template<class Classname>
	natural64 addCallback(
		void (Classname::*function)(ArgTypes...) noexcept,
		Classname* callobject)
	{
		m_PtrList.push_back(
			std::make_shared<
				ClassPtr<Classname, void, ArgTypes...>>(
				function, callobject));

		return m_PtrList.size() - 1;
	}

	//! \brief add a (member) function pointer to the
	//! delegate, to called when the others are
	//!
	//! \returns unsigned int indicating the position of the
	//! function in the delegate's array of functions
	//!
	//! \param[in] func shared_ptr to a classptr holding a
	//! (member) function pointer to be stored in this
	//! delegate
	template<class Classname>
	natural64 addCallback(
		std::shared_ptr<
			ClassPtr<Classname, void, ArgTypes...>>
			func)
	{
		runtime_assert(func.get() != nullptr);

		m_PtrList.push_back(func);
		return m_PtrList.size() - 1;
	}

	//! \brief remove a function from the delegate, at the
	//! specified index
	//!
	//! \param[in] index Index of the place in the array
	//! that the function to remove is stored
	void removeCallback(natural64 index)
	{
		runtime_assert(index < m_PtrList.size());

		m_PtrList.erase(index);
	}

	//! \brief gets number of stored functions, in this
	//! delegate
	//!
	//! \returns unsigned integer indicating the number of
	//! the functions in the delegate
	natural64 size() const noexcept
	{
		return m_PtrList.size();
	}

	//! \brief calls all functions in the delegate, using
	//! the supplied arguments
	void operator()(ArgTypes... args) noexcept
	{
		for (auto& ptr : m_PtrList)
		{
			ptr->operator()(args...);
		}
	}
};
}  // namespace frao

#endif  // !FRAO_PROTEAN_FUNCTOR_DELEGATE
