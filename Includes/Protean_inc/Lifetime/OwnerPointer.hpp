#ifndef FRAO_PROTEAN_LIFETIME_OWNER_POINTER
#define FRAO_PROTEAN_LIFETIME_OWNER_POINTER

#include "../Utility/DummyParam.hpp"
#include <algorithm>  //for std::sort
#include <Nucleus_inc/Environment.hpp>
#include <Nucleus_inc/Error.hpp>
#include <vector>  //for std::vector

//! \file
//!
//! \brief Header for OwnerPointer and AccessPointer classes
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

namespace frao
{
inline namespace Memory
{
template<typename DataType>
class OwnerPointer;

template<typename AccessType, typename DataType = AccessType>
class AccessPointer;

namespace IMPL
{
template<typename DataType>
class AssetPointer final
{
	// valid iff owner exists. null if not
	DataType* m_Data;
	// count of access pointers referencing the data
	frao::natural64 m_Lessees;
	bool			m_OwnerExists;

	template<typename RealType, typename... InitArgs>
	AssetPointer(DummyParam<RealType>, InitArgs&&... args)
		: m_Data(nullptr),
		  m_Lessees(0ULL),
		  m_OwnerExists(true)
	{
		try
		{
			// will convert types iff RealType derives from
			// DataType. If not, the code is wrong.
			m_Data = new RealType(
				std::forward<InitArgs>(args)...);
		} catch (std::bad_alloc&)
		{
			throw getErrorLog()->createException<MemAlloc>(
				FILELOC_UTF8, u8"");
		}
	}

	// no moving or copying
	AssetPointer(const OwnerPointer<DataType>&) = delete;
	AssetPointer(OwnerPointer<DataType>&&)		= delete;
	AssetPointer<DataType>& operator			=(
		const AssetPointer<DataType>&) = delete;
	AssetPointer<DataType>& operator			=(
		AssetPointer<DataType>&&) = delete;

   public:
	template<typename Other>
	friend class AssetPointer;

	~AssetPointer() noexcept
	{
		// this object should never be deleted if the owner
		// exists
		runtime_assert(!m_OwnerExists);

		SafeDelete(&m_Data);
	}

	// owner sells up, so to speak. Data will not longer be
	// accessible
	void divestOwnership() noexcept
	{
		SafeDelete(&m_Data);
		m_OwnerExists = false;
	}

	void leaseAccess()
	{
		++m_Lessees;
	}
	void endLeaseAccess()
	{
		--m_Lessees;
	}

	// get the underlying pointer, or nullptr if we have no
	// data. Can be called even when we have no asset
	DataType* getPtr() noexcept
	{
		return m_Data;
	}
	// get the underlying pointer, or nullptr if we have no
	// data. Can be called even when we have no asset
	const DataType* getPtr() const noexcept
	{
		return m_Data;
	}

	natural64 getLeasesCount() const noexcept
	{
		return m_Lessees;
	}

	DataType& operator*() noexcept
	{
		runtime_assert(m_Data != nullptr);

		return *m_Data;
	}
	const DataType& operator*() const noexcept
	{
		runtime_assert(m_Data != nullptr);

		return *m_Data;
	}
	DataType* operator->() noexcept
	{
		runtime_assert(m_Data != nullptr);

		return m_Data;
	}
	const DataType* operator->() const noexcept
	{
		runtime_assert(m_Data != nullptr);

		return m_Data;
	}

	bool hasReasonToExist() const
	{
		return m_OwnerExists || (m_Lessees != 0);
	}
	bool divestedFrom() const noexcept
	{
		return !m_OwnerExists;
	}

	explicit operator bool() const noexcept
	{
		return !divestedFrom();
	}

	template<typename RealType, typename... InitArgs>
	static AssetPointer<DataType>* makeAsset(
		InitArgs&&... args)
	{
		try
		{
			return new AssetPointer(
				DummyParam<RealType>{},
				std::forward<InitArgs>(args)...);
		} catch (std::bad_alloc&)
		{
			throw getErrorLog()->createException<MemAlloc>(
				FILELOC_UTF8, u8"");
		}
	}
};

}  // namespace IMPL

template<typename DataType>
class OwnerPointer final
{
	::frao::Memory::IMPL::AssetPointer<DataType>*
		m_Asset;  // valid ALWAYS while this object is valid
				  // (object is valid iff it has NOT been
				  // moved from)

	// no moving or copying
	OwnerPointer(const OwnerPointer<DataType>&) = delete;
	OwnerPointer<DataType>& operator			=(
		const OwnerPointer<DataType>&) = delete;

   public:
	template<typename Access, typename Data>
	friend class AccessPointer;

	// constuct invalid OwnerPointer
	OwnerPointer(std::nullptr_t = nullptr) noexcept
		: m_Asset(nullptr)
	{}
	template<typename RealDataType, typename... InitArgs>
	OwnerPointer(DummyParam<RealDataType>,
				 InitArgs&&... args)
		: m_Asset(
			::frao::Memory::IMPL::AssetPointer<DataType>::
				template makeAsset<RealDataType>(
					std::forward<InitArgs>(args)...))
	{}
	// original will be invalidated
	OwnerPointer(OwnerPointer<DataType>&& rhs) noexcept
		: m_Asset(rhs.m_Asset)
	{
		rhs.m_Asset =
			nullptr;  // invalidate original, so destructor
					  // (of rhs) doesn't fuck things up,
					  // later.
	}

	~OwnerPointer() noexcept
	{
		// ie: if this is still valid (has not been moved
		// from)
		if (m_Asset != nullptr)
		{
			m_Asset->divestOwnership();
		}
	}

	// rhs will be invalidated
	OwnerPointer<DataType>& operator=(
		OwnerPointer<DataType>&& rhs) noexcept
	{
		if (m_Asset != nullptr)
		{
			m_Asset->divestOwnership();
		}
		
		m_Asset = rhs.m_Asset;
		rhs.m_Asset =
			nullptr;  // invalidate original, so destructor
					  // (of rhs) doesn't fuck things up,
					  // later.

		return *this;
	}

	// will destroy the asset, and remove this object's
	// asset
	void sellAsset() noexcept
	{
		// ie: if this is still valid (has not been moved
		// from)
		if (m_Asset != nullptr)
		{
			m_Asset->divestOwnership();
			m_Asset = nullptr;
		}
	}

	// calling this function means that the specified
	// AccessPointer will be considered a lessee of the
	// asset. This means that the
	::frao::Memory::IMPL::AssetPointer<DataType>*
	leaseAsset() const
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");

		m_Asset->leaseAccess();

		return m_Asset;
	}

	natural64 getLeasesCount() const noexcept
	{
		return m_Asset->getLeasesCount();
	}

	bool hasAsset() const noexcept
	{
		return (m_Asset != nullptr) && (*m_Asset);
	}

	// get the underlying pointer, or nullptr if we have no
	// data. Can be called even when we have no asset
	DataType* getPtr() noexcept
	{
		if (m_Asset != nullptr)
		{
			return m_Asset->getPtr();
		}

		return nullptr;
	}
	// get the underlying pointer, or nullptr if we have no
	// data. Can be called even when we have no asset
	const DataType* getPtr() const noexcept
	{
		if (m_Asset != nullptr)
		{
			return m_Asset->getPtr();
		}

		return nullptr;
	}

	// not to be called if OwnerPointer is invalid or
	// empty!
	DataType& operator*() noexcept
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");

		return m_Asset->operator*();
	}
	// not to be called if OwnerPointer is invalid or
	// empty!
	const DataType& operator*() const noexcept
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");

		return m_Asset->operator*();
	}
	// not to be called if OwnerPointer is invalid or
	// empty!
	DataType* operator->() noexcept
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");
		return m_Asset->operator->();
	}
	// not to be called if OwnerPointer is invalid or
	// empty!
	const DataType* operator->() const noexcept
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");

		return m_Asset->operator->();
	}

	explicit operator bool() const noexcept
	{
		return hasAsset();
	}
};

// WARNING: creation of an object of this class is
// predicated upon user knowledge that the underlying asset
// is either actually an object of type AccessType, or else
// AccessType is a base class of the real type. If this is
// not the case, and the asset should NOT be treated as an
// object of type AccessType, the preconditions of this
// class are violated.
//
// NOTE: By default, DataType = AccessType. Not added here to avoid redefinition
template<typename AccessType, typename DataType>
class AccessPointer final
{
	::frao::Memory::IMPL::AssetPointer<DataType>* m_Asset;

	AccessPointer(
		::frao::Memory::IMPL::AssetPointer<DataType>* asset)
		: m_Asset(asset)
	{
		if (m_Asset != nullptr)
		{
			m_Asset->leaseAccess();
		}
	}

   public:
	static_assert(
		std::is_base_of_v<
			DataType,
			AccessType> || std::is_base_of_v<AccessType, DataType>,
		"AccessType must be either derived from, or the "
		"base of, DataType. This is a necessary, but not "
		"sufficient, criterion for the usage of this "
		"class");

	template<typename Access, typename Data>
	friend class AccessPointer;

	~AccessPointer() noexcept
	{
		if (m_Asset != nullptr)
		{
			endLease();
		}
	}

	// will construct empty access. Useless except in order
	// to assign to later
	AccessPointer() noexcept : m_Asset(nullptr)
	{}
	// will lease from specified owner
	AccessPointer(const OwnerPointer<DataType>& owner)
		: m_Asset(owner.leaseAsset())
	{
	}
	template<typename OtherAccess>
	AccessPointer(
		const AccessPointer<OtherAccess, DataType>&
			other) noexcept
		: AccessPointer(
			other
				.m_Asset)  // call asset pointer constructor
	{}
	// rhs will be invalidated
	template<typename OtherAccess>
	AccessPointer(AccessPointer<OtherAccess, DataType>&&
					  other) noexcept
		: m_Asset(other.m_Asset)
	{
		// don't need to update access count - we're
		// moving access from one object to another
		other.m_Asset = nullptr;
	}
	template<typename OtherAccess>
	AccessPointer<AccessType, DataType>& operator=(
		const AccessPointer<OtherAccess, DataType>& other)
	{
		if (this->m_Asset != other.m_Asset)
		{
			if (this->m_Asset != nullptr)
			{
				this->endLease();
			}

			this->m_Asset = other.m_Asset;

			if (this->m_Asset != nullptr)
			{
				this->m_Asset->leaseAccess();
			}

		} else if (other.m_Asset != nullptr)
		{
			other.endLease();
		}

		return *this;
	}
	// rhs will be invalidated
	template<typename OtherAccess>
	AccessPointer<AccessType, DataType>& operator=(
		AccessPointer<OtherAccess, DataType>&& other)
	{
		if (this->m_Asset != other.m_Asset)
		{
			if (this->m_Asset != nullptr)
			{
				this->endLease();
			}

			// don't need to update access count - we're
			// moving access from one object to another
			this->m_Asset = other.m_Asset;
			other.m_Asset = nullptr;

		} else if (other.m_Asset != nullptr)
		{
			other.endLease();
		}

		return *this;
	}
	AccessPointer<AccessType, DataType>& operator=(
		const OwnerPointer<DataType>& other)
	{
		if (this->m_Asset != other.m_Asset)
		{
			if (this->m_Asset != nullptr)
			{
				this->endLease();
			}

			this->m_Asset = other.leaseAsset();
		}

		return *this;
	}

	// remove this object's access to the asset it points to
	// (ie: make this empty)
	void endLease()
	{
		runtime_assert(m_Asset != nullptr);
		m_Asset->endLeaseAccess();

		if (!m_Asset->hasReasonToExist())
			SafeDelete(&m_Asset);
	}

	bool hasAsset() const noexcept
	{
		return (m_Asset != nullptr) && (*m_Asset);
	}
	bool scheduledForTermination() const noexcept
	{
		if (m_Asset != nullptr)
		{
			return m_Asset->scheduledForTermination();
		}

		return false;
	}

	// get the underlying pointer, or nullptr if we have no
	// data. Can be called even when we have no asset
	AccessType* getPtr() noexcept
	{
		if (m_Asset != nullptr)
		{
			return static_cast<AccessType*>(
				m_Asset->getPtr());
		}

		return nullptr;
	}
	// get the underlying pointer, or nullptr if we have no
	// data. Can be called even when we have no asset
	const AccessType* getPtr() const noexcept
	{
		if (m_Asset != nullptr)
		{
			return static_cast<const AccessType*>(
				m_Asset->getPtr());
		}

		return nullptr;
	}

	// not to be called if AccessPointer is invalid or
	// empty!
	AccessType& operator*() noexcept
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");

		return static_cast<AccessType&>(
			m_Asset->operator*());
	}
	// not to be called if AccessPointer is invalid or
	// empty!
	const AccessType& operator*() const noexcept
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");

		return static_cast<const AccessType&>(
			m_Asset->operator*());
	}
	// not to be called if AccessPointer is invalid or
	// empty!
	AccessType* operator->() noexcept
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");

		return static_cast<AccessType*>(
			m_Asset->operator->());
	}
	// not to be called if AccessPointer is invalid or
	// empty!
	const AccessType* operator->() const noexcept
	{
		runtime_assert(hasAsset(),
					   "Must have a valid asset");

		return static_cast<const AccessType*>(
			m_Asset->operator->());
	}

	explicit operator bool() const noexcept
	{
		return hasAsset();
	}
};

}  // namespace Memory
}  // namespace frao

#endif