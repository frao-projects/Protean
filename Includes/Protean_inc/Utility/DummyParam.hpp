#ifndef FRAO_PROTEAN_UTILITY_DUMMY_PARAM
#define FRAO_PROTEAN_UTILITY_DUMMY_PARAM

//! \file
//!
//! \brief Header of DummyParam class, for passing type info
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

namespace frao
{
//! \class DummyParam
//!
//! \brief class for passing dummy parameters (for type)
//! explicitly. Especially for constructors of template
//! classes that require type info. *Use as sparingly as
//! possible*
template<typename T>
class DummyParam
{
	using Type = T;
};
}  // namespace frao

#endif  //! FRAO_PROTEAN_UTILITY_DUMMY_PARAM