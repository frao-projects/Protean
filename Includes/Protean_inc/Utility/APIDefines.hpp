
//! \def PROTEAN_LIB_API
//!
//! \brief Will contain the necessary dll export or import declspec


#if defined _WIN32//Windows defines

//! \brief Designates that something is part of the library's API, and
//! should be exported
#if defined(PROTEAN_EXPORT)
#define PROTEAN_LIB_API __declspec(dllexport)
#elif defined(PROTEAN_IMPORT) //end PROTEAN_EXPORT section
#define PROTEAN_LIB_API __declspec(dllimport)
#else //end PROTEAN_IMPORT section
#define PROTEAN_LIB_API
#endif //end PROTEAN_LIB_API on windows section

#else//end windows

//On non-windows platforms, we don't have dlls, so define as nothing
#define PROTEAN_LIB_API

#endif