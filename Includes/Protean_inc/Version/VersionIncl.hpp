#ifndef FRAO_PROTEAN_VERSION_INCLUDE
#define FRAO_PROTEAN_VERSION_INCLUDE

//! \file
//!
//! \brief Header of version retreival functions, for this
//! project. Stores constexpr variables, and a function,
//! that encode the current Protean version
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <Nucleus_inc/Environment.hpp>
#include "ProteanVersion.h"
#include "../Utility/APIDefines.hpp"

namespace frao
{
inline namespace Environment
{
//! \brief version number, major component. a in "a.i.s.p.d"
constexpr const small_nat8 g_ProteanVersionMajor = VERSION_MAJOR;
//! \brief version number, minor component. i in "a.i.s.p.d"
constexpr const small_nat16 g_ProteanVersionMinor = VERSION_MINOR;
//! \brief version number, status component. s in "a.i.s.p.d"
constexpr const char g_ProteanVersionStatus[3] = VERSION_STATUS;
//! \brief version number, patch component. p in "a.i.s.p.d"
constexpr const small_nat16 g_ProteanVersionPatch = VERSION_PATCH;
//! \brief version number, development build component. d in
//! "a.i.s.p.d"
constexpr const small_nat16 g_ProteanVersionBuild =
	VERSION_DEV_BUILD;

//! \brief Get the Protean API version, for this build
//!
//! \returns A version information class, that holds the
//! version data, for this build
PROTEAN_LIB_API constexpr VersionInfo getProteanVersion() noexcept
{
	return VersionInfo(
		g_ProteanVersionMajor, g_ProteanVersionMinor,
		g_ProteanVersionPatch, g_ProteanVersionBuild, g_ProteanVersionStatus);
}
}  // namespace Environment
}  // namespace frao

//This will undef the macros we just used, second time around
#include "ProteanVersion.h"

#endif