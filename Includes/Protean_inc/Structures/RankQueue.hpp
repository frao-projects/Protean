#ifndef FRAO_PROTEAN_STRUCTURES_RANK_QUEUE
#define FRAO_PROTEAN_STRUCTURES_RANK_QUEUE

#include <Nucleus_inc/Environment.hpp>
#include <vector>  //for std::vector

//! \file
//!
//! \brief Header of RankQueue class, a container that that
//! ranks it's elements
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

namespace frao
{
inline namespace Structures
{
//! \class RankQueue
//!
//! \brief class that stores a series of elements that can
//! then be accessed in 'order' (for some user-defined
//! meaning of 'order'). Used for messaging, etc.
//!
//! \tparam ItemType Type of object that will stored in the
//! RankQueue
//!
//! \tparam Comparator functor which will be called with the
//! two objects, to determine which is first in order
//!
//! \todo cleanup Rankqueue functions, and make sure they
//! work as intended
//!
//! \todo add iterators (or maybe just const iterators?)
//!
//! \todo think about insertion (and erasure!) speed /
//! complexity wrt 'big-O'
//!
//! \todo add error handling?
template<typename ItemType, class Comparator>
class RankQueue
{
	class Item
	{
		ItemType m_Item;
		bool	 m_Valid;

	   public:
		Item() noexcept(noexcept(ItemType()))
			: m_Item(), m_Valid(false)
		{}

		Item& operator=(const ItemType& rhs) noexcept(
			noexcept(m_Item = rhs))
		{
			m_Item  = rhs;
			m_Valid = true;

			return *this;
		}

		const ItemType& getItem() const
			noexcept(noexcept(ItemType(m_Item)))
		{
			return m_Item;
		}
		ItemType claimItem() noexcept(
			noexcept(std::move(m_Item)))
		{
			m_Valid			= false;
			ItemType result = std::move(m_Item);

			return result;
		}

		void invalidate() noexcept
		{
			m_Valid = false;
		}
		explicit operator bool() const noexcept
		{
			return m_Valid;
		}
	};

	std::vector<Item>	  m_Queue;
	std::vector<natural64> m_Values;
	Comparator			   m_Comparator;

   public:
	//! \brief Construct empty queue, optionally passing a
	//! comparison functor
	//!
	//! \param[in] comp comparison functor object to use
	//! when comparing. Default initialised if not passed,
	//! so only useful to pass if functor has a non-trivial
	//! constructor
	RankQueue(Comparator comp = Comparator()) noexcept(
		noexcept(Item()) && noexcept(m_Comparator = comp)
		&& noexcept(Comparator()))
		: m_Queue(), m_Values(), m_Comparator(comp)
	{}

	//! \brief insert item into list, depending on it's
	//! order relative to the other elements
	//!
	//! \param[in] item The item to be inserted into the
	//! list
	//!
	//! \todo efficiently insert item! (merge sort?)
	void push(ItemType item)
	{
		natural64 placement = 0;

		// if the sizes are different, then there are
		// invalid queue members we can use
		if (m_Values.size() < m_Queue.size())
		{
			for (; placement < m_Queue.size(); ++placement)
			{
				// break if this element is empty
				if (!(m_Queue[placement])) break;
			}

			// shouldn't be possible if m_Values < m_Queue,
			// since that implies there are invalid members
			runtime_assert(placement != m_Queue.size());
		} else
		{
			// then we need to place the new element at the
			// end of the array
			placement = m_Queue.size();
			m_Queue.push_back(Item{});
		}

		// place new item at the end of the RankQueue
		m_Queue[placement] = item;
		m_Values.push_back(placement);

		// TODO: this doesn't work
		for (auto testeeIter = m_Values.rbegin(),
				  nextIter   = testeeIter + 1;
			 nextIter != m_Values.rend();
			 testeeIter = nextIter++)
		{
			// above: increment nextIter and set previous
			// value to testeeIter
			runtime_assert(testeeIter != m_Values.rend());
			runtime_assert((*testeeIter) < m_Queue.size());
			runtime_assert((*nextIter) < m_Queue.size());

			// see if the testee needs to move higher
			bool res =
				m_Comparator(m_Queue[*testeeIter].getItem(),
							 m_Queue[*nextIter].getItem());

			// if the testee should be higher, make it so,
			// and check against the next element. Otherwise
			// break
			if (res)
			{
				// swap places
				natural64 testeePlace = (*testeeIter);
				(*testeeIter)		  = (*nextIter);
				(*nextIter)			  = testeePlace;
			} else
			{
				break;
			}
		}
	}

	//! \brief get the next item from the list, removing it
	//! from the container
	//!
	//! \returns The 'top' item in the list, removing it
	//! from the container in the process
	ItemType pop()
	{
		// TODO: add error throwing

		natural64 place = m_Values.front();
		runtime_assert(place < m_Queue.size());

		// get the element
		ItemType result = m_Queue[place].claimItem();

		// remove previous top
		m_Values.erase(m_Values.begin());

		return result;
	}

	//! \brief indicates whether the container is non-empty
	//! or empty
	//!
	//! \returns boolean with value true if container is
	//! non-empty. false otherwise
	bool hasItems() const noexcept
	{
		return !(m_Values.empty());
	}

	//! \brief indicates the number of items that are
	//! currently in the list
	//!
	//! \returns an unsigned integer indicating the number
	//! of items
	natural64 numItems() const noexcept
	{
		return m_Values.size();
	}
};
}  // namespace Structures
}  // namespace frao

#endif  // !FRAO_PROTEAN_STRUCTURES_RANK_QUEUE
