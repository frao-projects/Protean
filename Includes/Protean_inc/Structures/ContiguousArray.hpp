#ifndef FRAO_PROTEAN_STRUCTURES_CONTIGUOUS_ARRAY
#define FRAO_PROTEAN_STRUCTURES_CONTIGUOUS_ARRAY

#include <array>  //for std::array
#include <Nucleus_inc/Allocate.hpp>
#include <Nucleus_inc/Environment.hpp>
#include <Nucleus_inc/Error.hpp>

//! \file
//!
//! \brief Header for Contiguous Array class
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

namespace frao
{
inline namespace Structures
{
namespace IMPL
{
// will create a std::array, and intialise all elements to
// startdata
template<typename Type, natural64 Elements>
std::array<Type, Elements> make_static_array(
	const Type& startdata)
{
	std::array<Type, Elements> result;
	result.fill(startdata);

	return result;
}

//! \class const_iterator
//!
//! \brief constant iterator class, of random access
//! category
//!
//! \tparam Container The container that the object will
//! iterate over. Must have a data member 'm_Data', which is
//! the ptr to the beginning of the data. And a data member
//! 'm_ArraySize', which is the the size of valid data, in
//! terms of number of 'Type' members of array
template<typename Container>
class const_iterator
{
   public:
	// stuff so that people (std:: algorithms etc), know
	// what to expect from this iterator class
	using iterator_category =
		std::random_access_iterator_tag;
	using value_type	  = typename Container::value_type;
	using difference_type = ptrdiff_t;
	using pointer	= const typename Container::value_type*;
	using reference = const typename Container::value_type&;

	//! \brief construct invalid const iterator
	constexpr const_iterator() noexcept
		: m_Ptr(nullptr), m_Container(nullptr)
	{}

	//! \brief explicitly construct const iterator from a
	//! location in a container. For use by container class
	//! this object iterates over
	explicit const_iterator(
		pointer ptr, const Container* container) noexcept
		: m_Ptr(ptr), m_Container(container)
	{
		if (m_Ptr == nullptr)
		{
			// if we don't have a pointer, then we should
			// construct an invalid iterator for this
			// container, and return
			return;
		}

		runtime_assert((m_Ptr - m_Container->data()) >= 0);
		runtime_assert(
			(m_Ptr - m_Container->data())
			< static_cast<integer64>(m_Container->size()));
	}

	//! \brief standard (defaulted) copy constructor
	const_iterator(const const_iterator&) noexcept =
		default;

	//! \brief standard (defaulted) move constructor
	const_iterator(const_iterator&&) noexcept = default;

	//! \brief virtual (defaulted) destructor, so that this
	//! class can be safely derived from
	virtual ~const_iterator() noexcept = default;

	//! \brief standard (defaulted) copy assignment operator
	//!
	//! \returns reference to *this, for chaining
	const_iterator& operator			=(
		const const_iterator&) noexcept = default;

	//! \brief standard (defualted) move assignment operator
	//!
	//! \returns reference to *this, for chaining
	const_iterator& operator=(const_iterator&&) noexcept =
		default;

	//! \brief standard 'dot' operator. Gets object pointed
	//! to by this iterator. Not to be called on an invalid
	//! iterator
	reference operator*() const noexcept
	{
		runtime_assert(m_Ptr != nullptr,
					   "Cannot dereference an "
					   "invalid iterator!");
		return *m_Ptr;
	}

	//! \brief standard 'arrow' operator. Gets members of
	//! object pointed to by this iterator. Not to be called
	//! on an invalid iterator
	pointer operator->() const noexcept
	{
		return m_Ptr;
	}

	//! \brief pre increment operator. Increment and return
	//! the new value
	//!
	//! \returns reference to this, for chaining
	const_iterator& operator++() noexcept
	{
		++m_Ptr;

		// check stuff, depending on debug mode
		validate();

		return *this;
	}

	//! \brief post increment operator. Increment and return
	//! the value from before the operation
	//!
	//! \returns new iterator, with previous value of *this
	const_iterator operator++(int) noexcept
	{
		const_iterator result = *this;

		++(*this);

		return result;
	}

	//! \brief pre decrement operator. Decrement and return
	//! the new value
	//!
	//! \returns reference to this, for chaining
	const_iterator& operator--() noexcept
	{
		--m_Ptr;

		// check stuff, depending on debug mode
		validate();

		return *this;
	}
	//! \brief post decrement operator. Decrement and return
	//! the value from before the operation
	//!
	//! \returns new iterator, with previous value of *this
	const_iterator operator--(int) noexcept
	{
		const_iterator result = *this;

		--(*this);

		return result;
	}

	//! \brief positively offset iterator, by the specified
	//! amount
	//!
	//! \returns new const iterator that points to the
	//! required location (or is invalid)
	//!
	//! \param[in] offset Offset this iterator, in a
	//! positive direction, by the specified amount
	const_iterator operator+(
		difference_type offset) const noexcept
	{
		const_iterator result(m_Ptr, m_Container);

		result += offset;

		return result;
	}

	//! \brief negatively offset iterator, by the specified
	//! amount
	//!
	//! \returns new const iterator that points to the
	//! required location (or is invalid)
	//!
	//! \param[in] offset Offset this iterator, in a
	//! negative direction, by the specified amount
	const_iterator operator-(
		difference_type offset) const noexcept
	{
		const_iterator result(m_Ptr, m_Container);

		result -= offset;

		return result;
	}

	//! \brief positively offset iterator, by the specified
	//! amount
	//!
	//! \returns reference to this, for chaining
	//!
	//! \param[in] offset Offset this iterator, in a
	//! positive direction, by the specified amount
	const_iterator& operator+=(
		difference_type offset) noexcept
	{
		m_Ptr += offset;

		validate();

		return *this;
	}

	//! \brief negatively offset iterator, by the specified
	//! amount
	//!
	//! \returns reference to this, for chaining
	//!
	//! \param[in] offset Offset this iterator, in a
	//! negative direction, by the specified amount
	const_iterator& operator-=(
		difference_type offset) noexcept
	{
		m_Ptr -= offset;

		validate();

		return *this;
	}

	//! \brief offset iterator by the specified amount
	//!
	//! \returns reference to *this, for chaining
	//!
	//! \param[in] offset Amount to offset the iterator by
	reference& operator[](
		difference_type offset) const noexcept
	{
		const_iterator newIter = (*this) + offset;
		runtime_assert(newIter.m_Ptr != nullptr);

		return *newIter;
	}

	//! \brief establish, for two iterator, whether they are
	//! equal, and if not, which is 'before' the other.
	//! Based on c++20's proposed 'spaceship' <=> operator
	//!
	//! \returns 0 iff the lhs and rhs are equal, a value <
	//! 0 if the lhs is less, and a value > 0 if the rhs is
	//! less.
	//!
	//! \param[in] rhs const reference to the iterator to
	//! compare on the rhs of the expression
	difference_type order(
		const const_iterator& rhs) const noexcept
	{
		if (this->m_Ptr != nullptr)
		{
			if (rhs.m_Ptr != nullptr)
			{
				runtime_assert(
					this->m_Container == rhs.m_Container,
					"Should only compare valid iterators "
					"that refer to the same container!");


				return rhs.m_Ptr - this->m_Ptr;
			} else
			{
				// then this is invalid, and rhs isn't. So
				// this is conceptually 'more' than rhs (so
				// that invalid iterators come after valid
				// ones)
				return 1;
			}
		} else
		{
			if (rhs.m_Ptr != nullptr)
			{
				// then this is invalid, and rhs isn't. So
				// this is conceptually 'more' than rhs (so
				// that invalid iterators come after valid
				// ones)
				return -1;
			} else
			{
				// then both are invalid - return 0
				return 0;
			}
		}
	}

	//! \brief determines if this iterator can be considered
	//! strictly 'before' the specifed rhs iterator.
	//!
	//! \returns boolean with value true iff *this < rhs
	//!
	//! \param[in] rhs const reference to value that should
	//! be used for the rhs of the expression
	bool operator<(const const_iterator& rhs) const noexcept
	{
		return order(rhs) < 0;
	}
	//! \brief determines if this iterator can be considered
	//! strictly 'after' the specifed rhs iterator.
	//!
	//! \returns boolean with value true iff *this > rhs
	//!
	//! \param[in] rhs const reference to value that should
	//! be used for the rhs of the expression
	bool operator>(const const_iterator& rhs) const noexcept
	{
		return order(rhs) > 0;
	}

	//! \brief determines if this iterator can be considered
	//! 'before' or equal to the specifed rhs iterator.
	//!
	//! \returns boolean with value true iff *this <= rhs
	//!
	//! \param[in] rhs const reference to value that should
	//! be used for the rhs of the expression
	bool operator<=(
		const const_iterator& rhs) const noexcept
	{
		return order(rhs) <= 0;
	}
	//! \brief determines if this iterator can be considered
	//! 'after' or equal the specifed rhs iterator.
	//!
	//! \returns boolean with value true iff *this >= rhs
	//!
	//! \param[in] rhs const reference to value that should
	//! be used for the rhs of the expression
	bool operator>=(
		const const_iterator& rhs) const noexcept
	{
		return order(rhs) >= 0;
	}

	//! \brief determines if this iterator can be considered
	//! equal to the specifed rhs iterator.
	//!
	//! \returns boolean with value true iff *this == rhs
	//!
	//! \param[in] rhs const reference to value that should
	//! be used for the rhs of the expression
	bool operator==(
		const const_iterator& rhs) const noexcept
	{
		return order(rhs) == 0;
	}

	//! \brief determines if this iterator can be considered
	//! unequal with the specifed rhs iterator.
	//!
	//! \returns boolean with value true iff *this != rhs
	//!
	//! \param[in] rhs const reference to value that should
	//! be used for the rhs of the expression
	bool operator!=(
		const const_iterator& rhs) const noexcept
	{
		return order(rhs) != 0;
	}

   protected:
	void validate() noexcept
	{
		runtime_assert(m_Ptr != nullptr,
					   "Cannot perform actions on "
					   "an invalid iterator!");
		runtime_assert(m_Container != nullptr);
		runtime_assert(
			m_Container->data() != nullptr,
			"Cannot perform actions on an iterator to an "
			"empty container!");

		ptrdiff_t diff = m_Ptr - m_Container->data();

		if (diff < 0)
		{
			m_Ptr = nullptr;
		} else if (diff >= static_cast<integer64>(
					   m_Container->size()))
		{
			m_Ptr = nullptr;
		}
	}

	pointer			 m_Ptr;
	const Container* m_Container;
};

//! \class iterator
//!
//! \brief iterator class, of random access category
//!
//! \tparam Container The container that the object will
//! iterate over. Must have a data member 'm_Data', which is
//! the ptr to the beginning of the data. And a data member
//! 'm_ArraySize', which is the the size of valid data, in
//! terms of number of 'Type' members of array
template<typename Container>
class iterator : public const_iterator<Container>
{
   public:
	// stuff so that people (std:: algorithms etc), know
	// what to expect from this iterator class
	using iterator_category =
		std::random_access_iterator_tag;
	using value_type	  = typename Container::value_type;
	using difference_type = ptrdiff_t;
	using pointer		  = typename Container::value_type*;
	using reference		  = typename Container::value_type&;

	using Base_Type = const_iterator<Container>;

	//! \brief construct invalid iterator
	constexpr iterator() noexcept : Base_Type()
	{}

	//! \brief explicitly create an iterator from a location
	//! in a container. For use by container classes
	//!
	//! \param[in] ptr the location in memory of the actual
	//! pointed to object
	//!
	//! \param[in] container the container that the iterator
	//! is iterating over
	explicit iterator(pointer		   ptr,
					  const Container* container) noexcept
		: Base_Type(ptr, container)
	{}

	//! \brief standard (defaulted) copy constructor
	iterator(const iterator&) noexcept = default;

	//! \brief standard (defaulted) move constructor
	iterator(iterator&&) noexcept = default;

	//! \brief virtual (defaulted) destructor, so that this
	//! class can be safely derived from
	virtual ~iterator() noexcept = default;

	//! \brief standard (defaulted) copy assignment operator
	//!
	//! \returns reference to *this, for chaining
	iterator& operator=(const iterator&) noexcept = default;

	//! \brief standard (defaulted) move assignment operator
	//!
	//! \returns reference to *this, for chaining
	iterator& operator=(iterator&&) noexcept = default;

	//! \brief standard 'dot' operator, to access the object
	//! this iterator points to. Must not be called on an
	//! invalid iterator
	reference operator*() const noexcept
	{
		runtime_assert(this->m_Ptr != nullptr,
					   "Cannot dereference an "
					   "invalid iterator!");

		return *const_cast<value_type*>(
			this->m_Ptr);  // const is a const_iterator
						   // thing, so get rid
	}

	//! \brief standard 'arrow' operator, to access members
	//! of this iterator's referenced object. Must not be
	//! called on an invalid iterator
	pointer operator->() const noexcept
	{
		runtime_assert(this->m_Ptr != nullptr,
					   "Cannot dereference an "
					   "invalid iterator!");

		return const_cast<value_type*>(this->m_Ptr);
	}

	//! \brief pre increment operator. Increment and return
	//! the new value
	//!
	//! \returns reference to this, for chaining
	iterator& operator++() noexcept
	{
		++(*static_cast<Base_Type*>(this));

		return *this;
	}
	//! \brief post increment operator. increment and return
	//! the value *prior* to the increment
	//!
	//! \returns new iterator, that points to the place this
	//! one does before the operation
	iterator operator++(int) noexcept
	{
		return (*static_cast<Base_Type*>(this))++;
	}

	//! \brief pre decrement operator. decrement and return
	//! the new value
	//!
	//! \returns reference to this, for chaining
	iterator& operator--() noexcept
	{
		--(*static_cast<Base_Type*>(this));

		return *this;
	}

	//! \brief post decrement operator. decrement and return
	//! the value *prior* to the decrement
	//!
	//! \returns new iterator, that points to the place this
	//! one does before the operation
	iterator operator--(int) noexcept
	{
		return (*static_cast<Base_Type*>(this))--;
	}

	//! \brief get new iterator 'offset' positions after
	//! this one
	//!
	//! \returns new iterator to the location in question
	//!
	//! \param[in] offset signed integer indicating offset
	//! (in the positive direction) from the element
	//! currently pointed to
	iterator operator+(
		difference_type offset) const noexcept
	{
		iterator result = *this;
		return (result += offset);
	}

	//! \brief get new iterator 'offset' positions before
	//! this one
	//!
	//! \returns new iterator to the location in question
	//!
	//! \param[in] offset signed integer indicating offset
	//! (in the negative direction) from the element
	//! currently pointed to
	iterator operator-(
		difference_type offset) const noexcept
	{
		iterator result = *this;
		return (result -= offset);
	}

	//! \brief positively offset iterator by the given
	//! amount
	//!
	//! \returns reference to *this, for chaining
	//!
	//! \param[in] offset signed integer indicating offset
	//! (in the positive direction) from the element
	//! currently pointed to
	iterator& operator+=(difference_type offset) noexcept
	{
		static_cast<Base_Type*>(this) += offset;

		return *this;
	}

	//! \brief negatively offset iterator by the given
	//! amount
	//!
	//! \returns reference to *this, for chaining
	//!
	//! \param[in] offset signed integer indicating offset
	//! (in the negative direction) from the element
	//! currently pointed to
	iterator& operator-=(difference_type offset) noexcept
	{
		static_cast<Base_Type*>(this) -= offset;

		return *this;
	}

	//! \brief offset iterator and dereference. Calling is
	//! invalid iff offset would cause iterator to be
	//! invalid
	//!
	//! \returns reference type of iterator (ie: probably
	//! Type& )
	//!
	//! \param[in] offset signed integer indicating offset
	//! from the element currently pointed to
	reference operator[](
		difference_type offset) const noexcept
	{
		return *((*this) + offset);
	}
};

}  // namespace IMPL

// a class to make a contiguous dynamic array of any
// dimensions
//! \class ContiguousArray
//!
//! \brief A multidimensional array that will always be
//! stored contiguously in memory. Used for either
//! performance (ie: cache etc.) purposes, or when an API
//! wants you to pass a buffer for multi-dimensional data
//! (say, a texture) but you want some abstraction and
//! convienience.
//!
//! \tparam Type The type of element in the array
//!
//! \tparam Dimensions The dimensionality of the array (2
//! for 2-D, 3 for 3-D etc.) **This is not the size(s) of
//! the dimensions.** This is just the number of dimensions
template<typename Type, natural64 Dimensions>
class ContiguousArray final
{
	Type*							  m_Data;
	std::array<natural64, Dimensions> m_DimensionSizes;
	natural64 m_ArraySize;	// useful size - 0 until members
							// have been constructed
	natural64 m_AllocatedElements;	// allocated size

	static_assert(Dimensions != 0,
				  "Cannot have 0 dimensional array!");

	natural64 distanceBetweenElementsOf(
		natural64 dimensionNum) const noexcept
	{
		natural64 result = 1, currentDimensionPlusOne =
								  m_DimensionSizes.size();

		// could use reverse iterators for this, but that
		// was slow, especially in debug builds
		while ((currentDimensionPlusOne > 0)
			   && ((currentDimensionPlusOne - 1)
				   > dimensionNum))
		{
			result *=
				m_DimensionSizes[currentDimensionPlusOne
								 - 1];

			--currentDimensionPlusOne;
		}

		return result;
	}
	template<typename... Args>
	static Type* createArray(
		std::array<natural64, Dimensions> size,
		natural64& arrSize, Args&&... args)
	{
		arrSize = 1;
		for (auto& dimSize : size)
		{
			arrSize *= dimSize;
		}

		// allocate memory, initialise as specified
		return AllocateArray<Type>(
			arrSize, std::forward<Args>(args)...);
	}
	// returns true if we succesfully copy constructed all
	// elements
	static bool copyConstructElementsInPlace(
		Type* writeBuffer, const Type* readBuffer,
		natural64 elements) noexcept
	{
		for (natural64 linearIndex = 0;
			 linearIndex < elements; ++linearIndex)
		{
			try
			{
				new (writeBuffer + linearIndex)
					Type(readBuffer[linearIndex]);
			} catch (...)
			{
				if (linearIndex > 0)
				{
					// then we managed to construct some
					// elements, which we should destroy,
					// before we throw
					destroyElements(&writeBuffer,
									linearIndex);

					return false;
				}
			}
		}

		return true;
	}
	static void destroyElements(Type**	  data,
								natural64 elements)
	{
		runtime_assert(data != nullptr);
		runtime_assert((*data) != nullptr);

		// iterate through constructed elements, in reverse
		// order, and destroy all of them
		for (natural64 reverseIndex = 0;
			 reverseIndex < elements; ++reverseIndex)
		{
			((*data) + (elements - reverseIndex - 1))
				->~Type();
		}

		FreeSpace(*data);
	}
	static auto getStrides(
		const std::array<natural64, Dimensions>&
			size) noexcept
		-> std::array<natural64, Dimensions>
	{
		std::array<natural64, Dimensions> result;
		auto rDestination = result.rbegin();

		natural64 last = 1;
		*rDestination  = last;
		++rDestination;

		// loop condition is vs result iterator, since that
		// is one closer to end
		for (auto rIter = size.rbegin();
			 rDestination != result.rend();
			 ++rIter, ++rDestination)
		{
			last = *rDestination = last * (*rIter);
		}

		return result;
	}
	static natural64 getLinearIndex(
		const std::array<natural64, Dimensions>& location,
		const std::array<natural64, Dimensions>&
			strides) noexcept
	{
		natural64 result = 0;

		for (natural64 index = 0; index < Dimensions;
			 ++index)
		{
			result += (location[index] * strides[index]);
		}

		return result;
	}

   public:
	using value_type = Type;
	using MyType	 = ContiguousArray<Type, Dimensions>;
	using iterator	 = IMPL::iterator<MyType>;
	using const_iterator = IMPL::const_iterator<MyType>;

	// all contiguous arrays are friends :D
	// we'd restrict it to the same underlying type, but c++
	// doesn't allow partial specialisation of friend class
	template<typename OtherType, natural64 OtherDimensions>
	friend class ContiguousArray;

	friend class IMPL::iterator<MyType>;
	friend class IMPL::const_iterator<MyType>;

	//! \brief Create null array, of 0 size
	ContiguousArray() noexcept
		: m_Data(nullptr),
		  m_DimensionSizes(
			  IMPL::make_static_array<natural64,
									  Dimensions>(0)),
		  m_ArraySize(0),
		  m_AllocatedElements(0)
	{}

	//! \brief construct array of specified size in each
	//! dimension (as specified), and then initialises each
	//! member of this array with the arguments provided
	//!
	//! \tparam Args parameter pack of arguments that will
	//! be supplied to constructor to create each element.
	//! Arguments must be copiable, since in order to create
	//! multiple elements with the same parameters, copies
	//! must be made
	//!
	//! \param[in] size array of size 'Dimensions' that has
	//! the size of the required array in each dimension.
	//! For example, a 3D array would supple an array of
	//! size 3: {x, y, z}, where x, y, and z are the sizes
	//! of the array wrt each dimension (0, 1, 2) = (MAJOR,
	//! MIDDLE, MINOR)
	//!
	//! \param args parameter pack that will be supplied to
	//! each element of the array's constuctor, on creation.
	//! All objects in the parameter pack should be
	//! copy-constructable, so that multipe elements can be
	//! supplied with the same arguments
	template<typename... Args>
	ContiguousArray(std::array<natural64, Dimensions> size,
					Args&&... args)
		: m_Data(nullptr),
		  m_DimensionSizes(std::move(size)),
		  m_ArraySize(0),
		  m_AllocatedElements(0)
	{
		// create with specified initialisation
		natural64 arrSize;
		m_Data		= createArray(m_DimensionSizes, arrSize,
							  std::forward<Args>(args)...);
		m_ArraySize = m_AllocatedElements = arrSize;
	}

	//! \brief Creates a subarray of another array. That is,
	//! it takes a ContiguousArray of dimensionality x, and
	//! creates one of dimensionality y (where y < x), using
	//! the data from the array of dimensionality x
	//!
	//! \tparam OtherDimensions the dimensionality of the
	//! array, whose data is used to construct this array.
	//! Must be that OtherDimensions > Dimensions
	//!
	//! \param[in] otherArray The array whose data will be
	//! used to construct this array. Has dimensionality
	//! 'OtherDimensions'
	//!
	//! \param[in] omittedDimensions array of size
	//! (OtherDimensions - Dimensions), that holds the
	//! information for which dimensions should be ignored
	//! (conceptually: removed) from 'otherArray' in order
	//! to create this sub-array, as well as what 'column'
	//! in the array to use for the data in this array. The
	//! array is made up of a series of pairs. The first
	//! element in each pair, is the dimension to omit. The
	//! second is what 'column' of data in that dimension,
	//! to take
	//!
	//! \exception InvalidUserInput throws if the data for
	//! what dimensions to use is inconsistent or impossible
	template<natural64 OtherDimensions>
	explicit ContiguousArray(
		const ContiguousArray<Type, OtherDimensions>&
			otherArray,
		std::array<std::pair<natural64, natural64>,
				   OtherDimensions - Dimensions>
			omittedDimensions)
		: ContiguousArray()
	{
		static_assert(OtherDimensions > Dimensions,
					  "Can only decrease dimensionality!");

		// we want size data in form {pair{keep, (keep) ?
		// size : 'row' to use}, ...}, so that we can
		// easily create subarray
		std::array<std::pair<bool, natural64>,
				   OtherDimensions>
			dimensionCopySpecs;

		// copy array, to form that will keep the info we
		// need, in order to copy old array data to this new
		// one
		for (natural64 dimensionIndex = 0;
			 dimensionIndex < OtherDimensions;
			 ++dimensionIndex)
		{
			dimensionCopySpecs[dimensionIndex] =
				std::make_pair(
					true,
					otherArray
						.m_DimensionSizes[dimensionIndex]);
		}

		// validate specified dimensions and dimension
		// coords. If NOT valid, throw
		for (auto referenceIter = omittedDimensions.begin();
			 referenceIter != omittedDimensions.end();
			 ++referenceIter)
		{
			// validate dimension index (ie: make sure we
			// have the specified dimension)
			if (referenceIter->first >= OtherDimensions)
			{
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"Specified Dimension to omit was greater than "_ustring
						u8"original dimensionality of the array"_ustring);
			}
			// validate 'row' of array we're going to keep
			// is in range. ie: if we're getting rid of
			// dimension 2, then we need to know which of
			// the elements (subarrays for 3rd, 4th
			// dimensions etc.) to keep. Say we decide to
			// keep element ('row') 6. But dimension 2 is
			// only 3 wide! This index was out of range for
			// the size of this dimension (in our example,
			// index is out of bounds of the 2nd dimension),
			// so we must throw, because we can't do that
			if (referenceIter->second
				>= otherArray.m_DimensionSizes[referenceIter
												   ->first])
			{
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"index of array section to keep was out of "_ustring
						u8"bounds of specified dimension!"_ustring);
			}

			// make sure we don't have duplicate dimensions
			// - compare this dimension against all
			// subsequent in the array - this will compare
			// all dimensions in the array
			for (auto testIter = referenceIter + 1;
				 testIter != omittedDimensions.end();
				 ++testIter)
			{
				// if we have two pairs that refer to the
				// same dimension, then dimensions were
				// invalid
				if (referenceIter->first == testIter->first)
				{
					throw getErrorLog()
						->createException<InvalidUserInput>(
							FILELOC_UTF8,
							u8"A dimension of the array was specified to "_ustring
							u8"be removed twice, when creating sub-array. "_ustring
							u8"This is impossible"_ustring);
				}
			}

			// now that we have a dimension to remove, put
			// that info in specs for copy
			dimensionCopySpecs[referenceIter->first] =
				std::make_pair(false,
							   referenceIter->second);
		}

		// construct New dimension sizes of result
		std::array<natural64, Dimensions> newArraySize;
		for (natural64 destIndex = 0, sourceIndex = 0;
			 destIndex < Dimensions;
			 ++destIndex, ++sourceIndex)
		{
			// find the next dimension to keep
			while (!dimensionCopySpecs[sourceIndex].first)
			{
				runtime_assert(sourceIndex
							   < OtherDimensions);
				++sourceIndex;
			}
			runtime_assert(sourceIndex < OtherDimensions);

			newArraySize[destIndex] =
				dimensionCopySpecs[sourceIndex].second;
		}

		// iterate over appropriate dimensions, and copy
		// construct elements of new array
		auto iterate =
			[](std::array<natural64, OtherDimensions>&
				   location,
			   const std::array<natural64, OtherDimensions>&
				   bounds) {
				// iterate location!
				auto locationIter = location.rbegin();
				auto boundsIter	  = bounds.rbegin();

				++(*locationIter);
				natural64 dimensionsChecked = 0;

				while (
					(dimensionsChecked < OtherDimensions)
					&& ((*locationIter) >= (*boundsIter)))
				{
					runtime_assert(locationIter
								   != location.rend());
					runtime_assert(boundsIter
								   != bounds.rend());

					if ((dimensionsChecked + 1)
						< OtherDimensions)
					{
						*locationIter = 0;
						*(locationIter + 1) += 1;
					}

					++dimensionsChecked;
					++locationIter;
					++boundsIter;
				}
			};

		// check whether location is one which,
		// according to our copy specs, should be copied
		auto inBounds =
			[&dimensionCopySpecs](
				const std::array<natural64,
								 OtherDimensions>&
					srcLocation) -> bool {
			for (natural64 dimensionIndex = 0;
				 dimensionIndex < OtherDimensions;
				 ++dimensionIndex)
			{
				if (dimensionCopySpecs[dimensionIndex]
						.first)
				{
					// ie: is the src location invalid
					// somehow?
					runtime_assert(
						srcLocation[dimensionIndex]
						< dimensionCopySpecs[dimensionIndex]
							  .second);
				} else
				{
					if (srcLocation[dimensionIndex]
						!= dimensionCopySpecs
							   [dimensionIndex]
								   .second)
					{
						return false;
					}
				}
			}

			return true;
		};

		// create Array (memory location) of appropriate
		// size
		natural64 arrSize = 1;
		for (auto& dimSize : newArraySize)
		{
			arrSize *= dimSize;
		}

		// create space for array, and if that succeeds (it
		// throws on fail), and we copy data successfully,
		// THEN set our data to not null
		Type* newArray =
			AllocateSpaceForArray<Type>(arrSize);
		natural64 constructedElements = 0;
		try
		{
			auto sourceLocation = IMPL::make_static_array<
				natural64, OtherDimensions>(0ULL);

			// iterate over src and copy only what we're
			// keeping
			for (const auto& srcElement : otherArray)
			{
				if (inBounds(sourceLocation))
				{
					new (&newArray[constructedElements])
						Type(srcElement);
					++constructedElements;
				}

				iterate(sourceLocation,
						otherArray.m_DimensionSizes);
			}
		} catch (...)
		{
			// Unwind (destroy) all elements we could
			// successfully copy (in reverse order)! And
			// then delete array space
			for (natural64 progressIndex = 0;
				 progressIndex < constructedElements;
				 ++progressIndex)
			{
				newArray[constructedElements - progressIndex
						 - 1]
					.~Type();
			}

			FreeSpace(newArray);
		}

		// now that we have successfully created array
		// (incl. element copy, we can set our member
		// variables
		m_Data				= newArray;
		m_DimensionSizes	= newArraySize;
		m_AllocatedElements = m_ArraySize = arrSize;
	}

	//! \brief Copy constructor: create a contiguous array
	//! that is an exact copy of an existing array
	//!
	//! \param[in] rhs Const reference to the source array,
	//! to copy from
	ContiguousArray(const MyType& rhs)
		: m_Data(nullptr),
		  m_DimensionSizes(rhs.m_DimensionSizes),
		  m_ArraySize(0),
		  m_AllocatedElements(0)
	{
		natural64 arrSize = rhs.size();

		if (arrSize == 0)
		{
			return;
		}

		Type* newData =
			AllocateSpaceForArray<Type>(arrSize);

		runtime_assert(arrSize == rhs.m_ArraySize);
		runtime_assert(newData != nullptr);

		if (copyConstructElementsInPlace(
				newData, rhs.m_Data, arrSize))
		{
			// then we successfully copied the data over

			m_Data		= newData;
			m_ArraySize = m_AllocatedElements = arrSize;
		} else
		{
			// then we failed to copy the data over
			FreeSpace(newData);

			throw getErrorLog()
				->createException<CreationFailure>(
					FILELOC_UTF8,
					u8"Failed to copy construct element of "
					u8"array! Constructor for an element "
					u8"threw!");
		}
	}

	//! \brief Move Constructor: create a contiguous
	//! array, using the internal structure of an
	//! exsiting contiguousarray. Will leave existing
	//! (rhs) array in an invalid / indeterminate state
	//!
	//! \param[in] rhs Rvalue reference to the object
	//! that should be used to construct the new one
	ContiguousArray(MyType&& rhs)
		: m_Data(rhs.m_Data),
		  m_DimensionSizes(rhs.m_DimensionSizes),
		  m_ArraySize(rhs.m_ArraySize),
		  m_AllocatedElements(rhs.m_AllocatedElements)
	{
		rhs.m_Data = nullptr;
		rhs.m_DimensionSizes =
			IMPL::make_static_array<natural64, Dimensions>(
				0);
		rhs.m_ArraySize			= 0;
		rhs.m_AllocatedElements = 0;
	}

	//! \brief destroy all elements (from back of array
	//! to front), and then deallocate the memory used
	//! for the array
	~ContiguousArray() noexcept
	{
		if (m_Data != nullptr)
		{
			destroyElements(&m_Data, m_ArraySize);
		}
	}

	//! \brief destroy existing data (as if destructor
	//! were called), and then create a copy of rhs
	//! using newly allocated memory
	//!
	//! \returns reference to *this, for chaining
	//!
	//! \param[in] rhs const reference to the object
	//! that should be copied from
	//!
	//! \todo Only dealloc and realloc if actually
	//! necessary
	//! - the existing buffer may be the correct size
	//! (will still obviously need to destroy all
	//! existing elements)
	ContiguousArray& operator=(const MyType& rhs)
	{
		natural64 arrSize = rhs.size();

		if (arrSize == 0)
		{
			if (m_Data != nullptr)
			{
				destroyElements(&m_Data, m_ArraySize);
			}

			m_DimensionSizes = IMPL::make_static_array<natural64,
									  Dimensions>(0);
			m_ArraySize = 0;
			m_AllocatedElements = 0;

			return *this;
		}

		Type* newData =
			AllocateSpaceForArray<Type>(arrSize);

		runtime_assert(arrSize == rhs.m_ArraySize);
		runtime_assert(newData != nullptr);

		if (copyConstructElementsInPlace(
				newData, rhs.m_Data, arrSize))
		{
			// then we successfully copied the data over.
			// destroy the old, if we had something
			if (m_Data != nullptr)
			{
				destroyElements(&m_Data, m_ArraySize);
			}

			m_DimensionSizes = rhs.m_DimensionSizes;
			m_Data			 = newData;
			m_ArraySize = m_AllocatedElements = arrSize;
		} else
		{
			// then we failed to copy the data over
			FreeSpace(newData);

			throw getErrorLog()
				->createException<CreationFailure>(
					FILELOC_UTF8,
					u8"Failed to copy construct element of "
					u8"array! Constructor for an element "
					u8"threw!");
		}

		return *this;
	}

	//! \brief destroy existing data (as if destructor
	//! were called), and then create an array using the
	//! internal state of the supplied (rhs) object. Rhs
	//! object will be left in an indeterminate /
	//! invalid state
	//!
	//! \returns reference to *this, for chaining
	//!
	//! \param[in] rhs Rvalue reference to object, whose
	//! internal state will now be used for this array
	ContiguousArray& operator=(MyType&& rhs)
	{
		if (m_Data != nullptr)
		{
			destroyElements(&m_Data, m_ArraySize);
		}
		m_Data				= rhs.m_Data;
		m_DimensionSizes	= rhs.m_DimensionSizes;
		m_ArraySize			= rhs.m_ArraySize;
		m_AllocatedElements = rhs.m_AllocatedElements;

		rhs.m_Data = nullptr;
		rhs.m_DimensionSizes =
			IMPL::make_static_array<natural64, Dimensions>(
				0);
		rhs.m_ArraySize			= 0;
		rhs.m_AllocatedElements = 0;

		return *this;
	}

	//! \brief get a pointer to the start of const
	//! buffer
	//!
	//! \returns const Type* where Type is the
	//! underlying type of this array
	const Type* data() const noexcept
	{
		return m_Data;
	}

	//! \brief get the size of this array, in number of
	//! elements of Type, where Type is the underlying
	//! type of the this array
	//!
	//! \returns unsigned 64-bit integer 'natural64'
	//! denoting the number of elements of type 'Type'
	//! in the array
	natural64 size() const noexcept
	{
		return m_ArraySize;
	}

	//! \brief returns the number of dimensions used to
	//! describe this array, That is, if this is a 2D,
	//! returns 2. If 3D: returns 3 , etc.
	//!
	//! \returns unsigned 64-bit integer 'natural64'
	//! denoting the number dimensions of this type. ie:
	//! 2 for 2D, 3 for 3D etc.
	constexpr natural64 dimensionality() const noexcept
	{
		return Dimensions;
	}

	//! \brief get the size of the array in a particular
	//! dimension. ie: for a 2D array of size {5, 3},
	//! passing values of 0 or 1 are valid, and will
	//! return 5 and 3 respectively
	//!
	//! \returns unsigned 64-bit integer 'natural64',
	//! denoting the size of the array in the given
	//! dimension
	//!
	//! \param[in] dimensionNum The index of the
	//! dimension whose size should returned. 0-indexed,
	//! so valid range is [0, Dimensions) or [0,
	//! Dimensions - 1]
	natural64 getDimensionSize(
		natural64 dimensionNum) const noexcept
	{
		runtime_assert(dimensionNum
					   < m_DimensionSizes.size());

		return m_DimensionSizes[dimensionNum];
	}

	//! \brief get the size of the array in every
	//! dimension
	//!
	//! \returns array of unsigned 64-bit integers
	//! 'natural64', with size Dimensions. Each element
	//! of the array will have the size of the
	//! respective dimensions of the array
	std::array<natural64, Dimensions> getDimensions()
		const noexcept
	{
		return m_DimensionSizes;
	}

	//! \brief get const iterator to beginning of array
	//!
	//! \returns ContiguousArray iterator type (random
	//! access) that points to the start of the array
	const_iterator cbegin() const noexcept
	{
		runtime_assert(m_Data != nullptr);

		return const_iterator(m_Data, this);
	}

	//! \brief const version of begin(). gets a
	//! const iterator to the start of the data
	//!
	//! \returns a const_iterator (random_access) that
	//! points to the beginning of this array (element
	//! {0,
	//! ..., 0})
	const_iterator begin() const noexcept
	{
		return cbegin();
	}

	//! \brief non-const version of begin(). gets an
	//! iterator to the start of the data
	//!
	//! \returns an iterator (random_access) that points
	//! to the beginning of this array (element {0,
	//! ..., 0})
	iterator begin() noexcept
	{
		runtime_assert(m_Data != nullptr);

		return iterator(m_Data, this);
	}

	//! \brief get const iterator to one past the end of
	//! the array
	//!
	//! \returns const_iterator (random access) that
	//! points (conceptually) to the one past the last
	//! element of the array
	const_iterator cend() const noexcept
	{
		return const_iterator(nullptr, this);
	}

	//! \brief const version of end(). gets a const
	//! iterator to one past the end of the array, for
	//! comparison with another iterator
	//!
	//! \returns const_iterator (random_access) that
	//! points (conceptually) to one past the last
	//! element of the array
	const_iterator end() const noexcept
	{
		return cend();
	}

	//! \brief non-const version of end(). get an
	//! iterator to one past the end of the array, for
	//! comparison with another iterator
	//!
	//! \returns iterator (random_access) that points
	//! (conceptually) to one past the last element of
	//! the array
	iterator end() noexcept
	{
		return iterator(nullptr, this);
	}

	//! \brief resize this array to a given size (in
	//! each dimension). Optionally copies existing data
	//! over as well
	//!
	//! \tparam CopyData should existing data be copied
	//! (conceptually: kept) after the resize option? On
	//! by default
	//!
	//! \tparam DefaultMemberArgs parameter pack for
	//! defauly array member construction arguments.
	//! Should not be used manually!
	//!
	//! \param[in] newSize dimensions that the array
	//! should have after resize
	//!
	//! \param[in] args parameter pack arguments, to use
	//! for all 'new' elements of the array, that cannot
	//! just be copied (because no corresponding 'old'
	//! element, or because copying is disabled)
	//!
	//! \todo add interpolation, if 'Type' supports it
	//! (can add and divide? pass functor / lamdba that
	//! interpolates between two passed 'Type' params?)
	template<bool CopyData = true,
			 typename... DefaultMemberArgs>
	void resize(std::array<natural64, Dimensions> newSize,
				DefaultMemberArgs&&... args)
	{
		natural64 arrSize = 1;
		for (auto& dimSize : newSize)
		{
			arrSize *= dimSize;
		}

		Type* newArray =
			AllocateSpaceForArray<Type>(arrSize);

		if constexpr (CopyData)
		{
			// ideal algorithm (real is this, but no
			// interpolation or functor) 	1) linear
			// iteration
			// over source 	2) get ND (source) location
			// 3) [option a] translate to (destination)
			// location ie: iterpolate 		[option b]
			// is (source) location in (destination)
			// bounds? [outcome a] if no, iterate
			// source, goto (1) 4) copy (possibly using
			// functor?) source -> desination, iterate
			// source, goto (1)

			auto iterateLocation =
				[](std::array<natural64, Dimensions>& loc,
				   const std::array<natural64, Dimensions>&
					   arrSize) noexcept {
					auto rDestIter	 = loc.rbegin();
					auto rSourceIter = arrSize.rbegin();

					// iterate location counter, and
					// rollover digits, if appropriate
					while (rDestIter != loc.rend())
					{
						++(*rDestIter);

						if ((*rDestIter) < (*rSourceIter))
						{
							break;
						}

						(*rDestIter) = 0;
						++rDestIter;
						++rSourceIter;
					}
				};
			auto inBounds =
				[](const std::array<natural64, Dimensions>&
					   loc,
				   const std::array<natural64, Dimensions>&
					   arrSize) noexcept -> bool {
				for (natural64 index = 0;
					 index < Dimensions; ++index)
				{
					if (loc[index] >= arrSize[index])
					{
						return false;
					}
				}

				return true;
			};

			// create 0-ed array, to serve as current
			// location
			auto destinationLocation =
				IMPL::make_static_array<natural64,
										Dimensions>(0ULL);
			auto sourceStrides =
				getStrides(m_DimensionSizes);

			for (natural64 destinationIndex = 0;
				 destinationIndex < arrSize;
				 ++destinationIndex)
			{
				if (inBounds(destinationLocation,
							 m_DimensionSizes))
				{
					// then get linear offset from
					// m_Data pointer to actual
					// readlocation
					natural64 sourceIndex = getLinearIndex(
						destinationLocation, sourceStrides);

					// construct using move constructor
					// of type
					new (newArray + destinationIndex) Type(
						std::move(m_Data[sourceIndex]));
					// ie similar to:
					// newArray[destinationIndex] =
					// m_Data[sourceIndex];
				} else
				{
					new (newArray + destinationIndex) Type(
						std::forward<DefaultMemberArgs>(
							args)...);
					// ie similar to:
					// newArray[destinationLocation] =
					// Type(std::forward<DefaultMemberArgs>(args)...);
				}

				iterateLocation(destinationLocation,
								newSize);
			}
		} else
		{
			// default construct all!
			for (natural64 index = 0; index < arrSize;
				 ++index)
			{
				new (newArray + index)
					Type(std::forward<DefaultMemberArgs>(
						args)...);
			}
		}

		if (m_Data != nullptr)
		{
			destroyElements(&m_Data, m_ArraySize);
		}
		m_Data		= newArray;
		m_ArraySize = m_AllocatedElements = arrSize;
	}

	//! \brief create a subarray of this, with a reduced
	//! dimensionality. Corresponding elements will be
	//! copied over, and for discarded dimensions, a
	//! 'row' to use must be specified
	//!
	//! \tparam NewDimensions The dimensionality of the
	//! subarray. Must be strictly smaller than the
	//! dimensionality of this array
	//!
	//! \param[in] omittedDimensionCoords Array of pairs
	//! that denotes what dimensions to remove from the
	//! sub-array (as compared to this array).
	//! pair.first is the dimension that should be
	//! removed. pair.second is what 'row' to use, for
	//! the omitted dimension.
	//!
	//! \exception InvalidUserInput throws if the data
	//! for what dimensions to use is inconsistent or
	//! impossible
	template<natural64 NewDimensions>
	auto createSubArrayCopy(
		std::array<std::pair<natural64, natural64>,
				   Dimensions - NewDimensions>
			omittedDimensionCoords) const
		-> ContiguousArray<Type, NewDimensions>
	{
		static_assert(NewDimensions < Dimensions,
					  "Sub Arrays must be smaller than the "
					  "original!");

		return ContiguousArray<Type, NewDimensions>(
			*this, std::move(omittedDimensionCoords));
	}

	//! \brief provides *unchecked* access to the
	//! element at the given location in the array
	//!
	//! \returns const reference to the element in
	//! question
	//!
	//! \param[in] location 0-indexed array denoting
	//! location of element to return
	const Type& getDataAt(std::array<natural64, Dimensions>
							  location) const noexcept
	{
		// call non-const version, and return what that
		// gets (but converted to const)
		return const_cast<MyType&>(*this).getDataAt(
			std::move(location));
	}

	//! \brief provides *unchecked* access to the
	//! element at the given location in the array
	//!
	//! \warning If touching every/many elements of the
	//! array, this function is much slower that iterating
	//! linearly using iterators
	//!
	//! \returns reference to the element in question
	//!
	//! \param[in] location 0-indexed array denoting
	//! location of element to return
	Type& getDataAt(
		std::array<natural64, Dimensions> location) noexcept
	{
		natural64 elementNum = 0;

		for (natural64 dimensionIndex = 0;
			 dimensionIndex < location.size();
			 ++dimensionIndex)
		{
			// check that location is within bounds
			runtime_assert(
				location[dimensionIndex]
				< m_DimensionSizes[dimensionIndex]);

			elementNum +=
				distanceBetweenElementsOf(dimensionIndex)
				* location[dimensionIndex];
		}

		return m_Data[elementNum];
	}

	//! \brief provides *unchecked* access to the
	//! element at the given location in the array
	//!
	//! \warning If touching every/many elements of the
	//! array, this function is much slower that iterating
	//! linearly using iterators
	//!
	//! \returns const reference to the element in
	//! question
	//!
	//! \param[in] location 0-indexed array denoting
	//! location of element to return
	const Type& operator[](std::array<natural64, Dimensions>
							   location) const noexcept
	{
		return getDataAt(std::move(location));
	}

	//! \brief provides *unchecked* access to the
	//! element at the given location in the array
	//!
	//! \warning If touching every/many elements of the
	//! array, this function is much slower that iterating
	//! linearly using iterators
	//!
	//! \returns reference to the element in question
	//!
	//! \param[in] location 0-indexed array denoting
	//! location of element to return
	Type& operator[](
		std::array<natural64, Dimensions> location) noexcept
	{
		return getDataAt(std::move(location));
	}

	//! \brief returns whether this a valid array. That
	//! is, is the array has data. That is, of non-0
	//! length
	explicit operator bool() const noexcept
	{
		// Do we NEED the second check?
		return (m_Data != nullptr) && (m_ArraySize != 0);
	}
};
}  // namespace Structures
}  // namespace frao

#endif	// !FRAO_PROTEAN_STRUCTURES_CONTIGUOUS_ARRAY
