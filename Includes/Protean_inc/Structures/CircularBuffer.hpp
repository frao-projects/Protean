#ifndef FRAO_PROTEAN_STRUCTURES_CIRCULAR_BUFFER
#define FRAO_PROTEAN_STRUCTURES_CIRCULAR_BUFFER

#include <Nucleus_inc/Allocate.hpp>
#include <Nucleus_inc/Environment.hpp>
#include <Nucleus_inc/Error.hpp>

//! \file
//!
//! \brief Header for Circular Buffer class
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

namespace frao
{
inline namespace Structures
{
//! \class CircularBuffer
//!
//! \brief Class that stores elements of the specified type.
//! object will store a user (runtime specified) number of
//! objects, in an arrangement such that old elements are
//! replaced by new ones, once the specified size has been
//! reached. That is, the allocated buffer will be
//! functionally circular, to he perspective of a user
//!
//! \todo add iterators, const_iterators and
//! reverse_iterators to the class, and have funcitons that
//! use them (in addition to the existing functions,
//! probably? Or maybe since indexes are kinda.. janky for a
//! circular buffer [in that elements' positions will change
//! on adding an element], we should remove the index
//! functions?)
template<class DataType>
class CircularBuffer
{
	// INVARIANT: m_Start and m_Write must be left in valid
	// positions!

	DataType* m_Memory;		 // CHANGE TO void* FOR MALLOC /
							 // PLACEMENT NEW?
	const natural64 m_Size;  // in elements of DataType!
	natural64		m_Start;  // oldest (earliest) element
	natural64		m_Write;  // next element to write to

   public:
	//! \brief Construct a CircularBuffer, which can hold
	//! the specified number of elements
	//!
	//! \param[in] size The number of elements that the
	//! buffer should be able to hold
	CircularBuffer(natural64 size)
		: m_Memory(nullptr),
		  m_Size(size + 1) /* + 1 because one element is
							  always empty*/
		  ,
		  m_Start(0),
		  m_Write(0)
	{
		static_assert(
			sizeof(DataType) != 0,
			"DataTypes in CircularBuffer<DataType> must be "
			"at least of size 1");
		runtime_assert(
			m_Size
			> 1);  // we need at least one empty element...

		m_Memory = AllocateSpaceForArray<DataType>(m_Size);
	}

	//! \brief destroy circularbuffer, destroying all
	//! elements and deallocating memory used
	//!
	//! \todo make the buffer destroy elements in
	//! **reverse** order
	~CircularBuffer() noexcept
	{
		// TODO: deconstruct in REVERSE ORDER
		natural64 index = m_Start;

		while (index != m_Write)
		{
			DataType* element = m_Memory + index;
			element->~DataType();

			++index;
			if (index >= m_Size) index -= m_Size;
		}

		FreeSpace(m_Memory);
	}

	//! \brief will construct, using placement new, an
	//! element at the next position in the buffer. If this
	//! fills the buffer, the oldest element will be
	//! destroyed
	//!
	//! \tparam Types parameter pack that holds the
	//! arguments types of the constructor to call, when
	//! constructing the element
	//!
	//! \returns index of the element created
	//!
	//! \param[in] args parameter pack arguments that will
	//! be passed to the constructor (via std::forward),
	//! when constructing the element
	template<class... Types>
	natural64 append(Types&&... args)
	{
		const natural64 result = m_Write;
		runtime_assert(m_Write < m_Size);

		DataType* data = m_Memory + m_Write;
		try
		{
			// construct in place
			new (data)
				DataType(std::forward<Types>(args)...);
		} catch (...)
		{
			throw getErrorLog()->createException<MemAlloc>(FILELOC_UTF8, u8"");
		}

		++m_Write;

		if (m_Write >= m_Size) m_Write -= m_Size;

		if (m_Write == m_Start)
		{
			data = m_Memory + m_Write;
			data->~DataType();

			++m_Start;
			if (m_Start >= m_Size) m_Start -= m_Size;
		}

		return result;
	}

	//! \brief destroy the oldest element in the buffer
	void pop()
	{
		if (m_Start != m_Write)
		{
			// then we actually have something at start to
			// remove
			auto dataloc = m_Memory + m_Write;
			dataloc->~DataType();
			++m_Start;

			if (m_Start >= m_Size) m_Start -= m_Size;
		}
	}

	//! \brief gets the element at the specified location in
	//! the circular buffer. **Index is relative to the
	//! conceptual start of the buffer, not the start in
	//! memory.** Returns last element, if index is out of
	//! scope
	//!
	//! \returns const reference to the element in question
	//!
	//! \param[in] element index of the element to return.
	//! Index is measured relative to conceptual start of
	//! the array (not start in memory)
	const DataType& get(natural64 element) const noexcept
	{
		natural64 num, offset;

		if (m_Write < m_Start)
			num = (m_Write + m_Size) - m_Start;
		else
			num = m_Write - m_Start;

		if (element >= num)
		{
			// requested element is out of scope; return
			// last (most recent) element

			if (m_Write != 0)
				offset = m_Write - 1;
			else
				offset = m_Size - 1;
		} else
		{
			offset = m_Start + element;

			if (offset >= m_Size) offset -= m_Size;
		}

		return *(m_Memory + offset);
	}

	//! \brief returns the number of elements, that the
	//! buffer has room for
	//!
	//! \returns number indicating the number of elements
	//! that can be stored in the buffer simultaneously
	natural64 size() const noexcept
	{
		return m_Size - 1;
	}

	//! \brief returns the current size of the buffer, in
	//! terms of number of elements that are currently
	//! stored (not buffer size)
	//!
	//! \returns number of elements that the buffer is
	//! currently storing
	natural64 runningSize() const noexcept
	{
		return (m_Write < m_Start)
				   ? (m_Write + m_Size) - m_Start
				   : (m_Write - m_Start);
	}

	//! \brief directly index buffer (**unchecked**), as
	//! incremented from conceptual start of buffer (not
	//! start in memory)
	//!
	//! \returns const reference to the specified element.
	//! validity unchecked
	//!
	//! \param[in] element index of element to return, as
	//! incremented from conceptual start of buffer. *index
	//! is unchecked*
	const DataType& operator[](natural64 element) const
		noexcept
	{
		natural64 offset = m_Start + element;

		if (offset >= m_Size) offset -= m_Size;

		return *(m_Memory + offset);
	}
};
}  // namespace Structures
}  // namespace frao

#endif  // !FRAO_PROTEAN_STRUCTURES_CIRCULAR_BUFFER
