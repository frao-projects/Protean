#include "Functor/Delegate.hpp"
#include "Structures/CircularBuffer.hpp"
#include "Structures/ContiguousArray.hpp"
#include "Structures/RankQueue.hpp"
#include "Utility/DummyParam.hpp"
#include "Version/VersionIncl.hpp"

// this file exists to make sure we have source files, and
// to ensure that every header has been compiled at least
// once, because otherwise this project is header only